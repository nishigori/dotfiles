setlocal autoindent
setlocal formatoptions=tcq2l
setlocal textwidth=78 shiftwidth=4
setlocal softtabstop=4 tabstop=8
setlocal expandtab
setlocal foldmethod=syntax
setlocal foldlevel=1
